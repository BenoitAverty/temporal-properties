package com.zenika.temporal;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class Temporal<V> {
    TreeMap<Instant, V> treeMap = new TreeMap<>();

    public Temporal() {
    }

    public Temporal(V initialValue) {
        this.treeMap.put(Instant.MIN, initialValue);
    }

    public Optional<V> getValueAt(Instant when) {
        return Optional.ofNullable(treeMap.floorEntry(when))
                .map(Map.Entry::getValue);
    }

    public void clearFrom(Instant instantOfClear) {
        this.treeMap.put(instantOfClear, null);
    }

    public void put(V value, Instant start) {
        this.treeMap.put(start, value);
    }
}
