package com.zenika.temporal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Optional;

class TemporalTest {

    @Test
    void testEmptyTemporal() {
        Temporal<String> presidents = new Temporal<>();

        Assertions.assertEquals(Optional.empty(), presidents.getValueAt(Instant.now()));
    }

    @Test
    void testWithOneValue() {
        Temporal<String> presidents = new Temporal<>("Georges Pompidou");

        Assertions.assertEquals(Optional.of("Georges Pompidou"), presidents.getValueAt(Instant.now()));
    }

    @Test
    void testOneValueUntilDate() {
        Temporal<String> presidents = new Temporal<>("Georges Pompidou");
        presidents.clearFrom(Instant.ofEpochSecond(100));

        Assertions.assertEquals(Optional.empty(), presidents.getValueAt(Instant.ofEpochSecond(200)));
        Assertions.assertEquals(Optional.of("Georges Pompidou"), presidents.getValueAt(Instant.ofEpochSecond(50)));
    }

    @Test
    void testOneValueFromDate() {
        Instant reference = Instant.now();

        Temporal<String> presidents = new Temporal<>();
        presidents.put("Georges Pompidou", reference);

        Assertions.assertEquals(Optional.empty(), presidents.getValueAt(reference.minusSeconds(100)));
        Assertions.assertEquals(Optional.of("Georges Pompidou"), presidents.getValueAt(reference.plusSeconds(180)));
    }

    @Test
    void testOneValueFromDateUntilOtherDate() {
        Instant reference = Instant.now();

        Temporal<String> presidents = new Temporal<>();
        presidents.put("Georges Pompidou", reference.minusSeconds(100));
        presidents.clearFrom(reference.plusSeconds(100));

        Assertions.assertEquals(Optional.empty(), presidents.getValueAt(reference.minusSeconds(150)));
        Assertions.assertEquals(Optional.of("Georges Pompidou"), presidents.getValueAt(reference));
        Assertions.assertEquals(Optional.empty(), presidents.getValueAt(reference.plusSeconds(150)));
    }
}